import { config } from "./global";

export const Fields = {
  name: {
    name: "Name",
    field: "Name",
    class: config.classes.bats,
    asc: true,
    checked: true
  },
  rank: {
    name: "Rank",
    field: "Rank",
    class: "col-md-1",
    asc: true,
    checked: true
  },
  raffs: {
    name: "Raffs",
    field: "Raffs",
    class: config.classes.bats,
    asc: true,
    checked: true
  },
  mines: {
    name: "Mines",
    field: "Mines",
    class: config.classes.bats,
    asc: true,
    checked: true
  },
  infos: {
    name: "Infos",
    field: "Infos",
    class: config.classes.bats,
    asc: true,
    checked: true
  },
  usines: {
    name: "Usines",
    field: "Usines",
    class: config.classes.bats,
    asc: true,
    checked: true
  },
  munis: {
    name: "Munis",
    field: "Munis",
    class: config.classes.bats,
    asc: true,
    checked: true
  },
  connected: {
    name: "Connected",
    field: "Co",
    class: "col-md-1",
    asc: true,
    checked: true
  }
};
