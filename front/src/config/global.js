import { Charts } from './charts'

const prod = 'ntm314.pink/api/';
const dev = 'localhost:8013/';

if (dev) {
	(function (e) { return e }) ('e');
}

export const config = {
  classes: {
    bats: ""
	},
	url: prod,
	Charts,
};