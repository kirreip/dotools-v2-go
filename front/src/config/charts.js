export const Charts = [
  {
    name: 'Mines',
    color: '#a29bfe',
    type: 'dual',
    fields: ['BatsMines']
  },
  {
    name: 'Raffineries',
    color: '#00b894',
    type: 'dual',
    fields: ['BatsRaffs']
  },
  {
    name: 'Usines',
    color: '#fd79a8',
    type: 'dual',
    fields: ['BatsUsines']
  },
  {
    name: 'Usines de munitions',
    color: '#e84393',
    type: 'dual',
    fields: ['BatsMunis']
  },
  {
    name: 'Points',
    color: '#00cec9',
    type: 'dual',
    fields: ['Points']
  },
  {
    name: 'Rank',
    color: '#0984e3',
    type: 'mono',
    fields: ['Rank']
  },
  {
    name: 'RankCombat',
    color: '#74b9ff',
    type: 'mono',
    fields: ['RankCombat']
  },
  {
    name: 'DeltaPoint',
    color: '#fdcb6e',
    type: 'mono',
    fields: ['DeltaPoint']
  },
  {
    name: 'Victoires',
    color: '#fd79a8',
    type: 'mono',
    fields: ['Win']
  },
  {
    name: 'Défaites',
    color: '#e17055',
    type: 'mono',
    fields: ['Lose']
  }
];
