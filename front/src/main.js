import Vue from "vue";
import App from "./App.vue";
import StatsPlayer from "./components/StatsPlayer.vue";
import ListPlayers from "./components/ListPlayers.vue";
import Dashboard from "./components/Dashboard.vue";
import VueMaterial from "vue-material";
import VueRouter from "vue-router";
import "vue-material/dist/vue-material.min.css";
import "./assets/theme.scss";

import { config } from "./config/global";
import Axios from 'axios';
import Moment from 'moment';
import Chart from "chart.js";
import "moment-locale-fr";

Vue.prototype.$config = config;
Vue.prototype.$http = Axios;
Vue.prototype.$moment = Moment;
Vue.prototype.$chart = Chart;

Vue.use(VueMaterial);
Vue.use(VueRouter);
const routes = [
  {
    path: '/dashboard',
    component: Dashboard,
  },
  {
    path: '/admin',
    redirect: '/dashboard',
  },
  {
    path: "/listPlayers",
    name: "listPlayers",
    component: ListPlayers,
    props(route) { 
      return route.query || {};
    },
    children: [
    ]
  },
  { path: "/statsPlayer/:id", name: "statsPlayer", component: StatsPlayer }
];

const router = new VueRouter({
  routes,
  mode: 'history'
});

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  router
}).$mount("#app");
