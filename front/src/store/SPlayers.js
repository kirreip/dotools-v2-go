export const SPlayers = {
	debug: false,
	state: {
		players: [],
		request: {
			start: '0',
			end: '20',
		},
		statsPlayers:{
			test: '',
		}
	},
	setPlayers(newPlayers) {
		if (this.debug) {
			console.log(newPlayers);
		}
		this.state.players = newPlayers;
	},
	clearPlayers() {
		this.state.players = [];
	},
};