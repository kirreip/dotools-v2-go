package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/influxdata/influxdb/client/v2"
)

const db = "users"

var c, err = client.NewHTTPClient(client.HTTPConfig{
	Addr:     "http://localhost:8086",
	Username: "test",
	Password: "test2314",
})

// queryDB convenience function to query the database
func queryDB(clnt client.Client, cmd string) (res []client.Result, err error) {
	q := client.Query{
		Command:  cmd,
		Database: db,
	}
	if response, err := clnt.Query(q); err == nil {
		if response.Error() != nil {
			return res, response.Error()
		}
		res = response.Results
	} else {
		return res, err
	}
	return res, nil
}

func testHandle(w http.ResponseWriter, r *http.Request) {
	if (*r).Method == "OPTIONS" {
		return
	}
	fmt.Printf("%s\n\n\n\n\n\n\n\n", (*r).RequestURI)
}

func playersList(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	if (*r).Method == "OPTIONS" {
		return
	}
	var request struct {
		Start int
		End   int
	}
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		fmt.Fprint(w, err)
	}
	err = json.Unmarshal(body, &request)
	if request.End <= request.Start || request.End < 0 || request.Start < 0 {
		fmt.Fprint(w, errors.New("Bad inputs"))
	}
	tabRet := make([]infos, 0, (request.End - request.Start))
	m.Range(func(k, vTmp interface{}) bool {
		v := vTmp.(infos)
		if v.Rank < request.End && v.Rank > request.Start {
			tabRet = append(tabRet, v)
		}
		return true
	})
	ret, err := json.Marshal(tabRet)
	if err != nil {
		fmt.Fprint(w, err)
	}
	w.Write(ret)
}

func playersGetInfos(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	fmt.Println((*r).RequestURI)
	if (*r).Method == "OPTIONS" {
		return
	}
	vars := mux.Vars(r)
	var request struct {
		ID string `json:"id"`
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprint(w, err)
	}
	err = json.Unmarshal(body, &request)
	if err != nil {
		fmt.Fprint(w, err)
	}
	res, err := queryDB(c, fmt.Sprintf("SELECT * FROM \"%s\" LIMIT %d", vars["id"][:10], 1000))
	if err != nil {
		fmt.Fprint(w, err)
	}
	ret, err := json.Marshal(res)
	if err != nil {
		fmt.Fprint(w, err)
	}
	w.Write(ret)
}
