package logs

import (
	"math"
	"strconv"
)

// Log is simply a float64
type Log float64

// TransformStringIntoLogs is taking a string and parse it into a Log
func TransformStringIntoLogs(nb string) Log {
	len := len(nb)
	var ret Log
	if len < 7 {
		float, _ := strconv.ParseFloat(nb, 64)
		ret = Log(float)
	} else {
		float, _ := strconv.ParseFloat(nb[:7], 64)
		ret = Log(math.Log(float))
		ret += Log(len) - 7
	}
	return ret
}

//AddLogs Addition for Log
func AddLogs(a Log, b Log) Log {
	max := math.Max(float64(a), float64(b))
	min := math.Min(float64(a), float64(b))
	diff := min - max
	if diff > 10 {
		return Log(max)
	}
	return Log(max + math.Log(1+math.Pow(math.E, diff)))
}
