package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

var thisUsersHash = "4bea631d85e79675914653bbdc30af56"
var sessID = "PHPSESSID=c99efeaf2764b3c017191fed3abdca1d; thisUsersLandId=564532; ; thisUsersHash="
var cookie = sessID + thisUsersHash

type state struct {
	State       bool
	NbPlayers   int
	CurrentHash string
}

func _getState() state {
	state := state{
		config.isValidState,
		config.nbPlayers,
		thisUsersHash,
	}
	return state
}

func setCookie(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	if (*r).Method == "OPTIONS" {
		return
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
	}
	if len(body) < 1 {
		w.Write([]byte("Bad request"))
		return
	}
	var newCookie struct {
		UserHash string
	}
	err = json.Unmarshal(body, &newCookie)
	if err != nil {
		log.Fatal(err)
	}
	thisUsersHash = newCookie.UserHash
	cookie = sessID + thisUsersHash
	fmt.Println("Try with: ", cookie)
	makeIDTab(config.start, config.start+config.nbPlayers/2)
	fmt.Println("First part rebuilt")
	makeIDTab(config.start+config.nbPlayers/2-1, config.nbPlayers)
	fmt.Println("Second part rebuilt")
	config.isValidState = true
	state := _getState()
	if stateJSON, err := json.Marshal(state); err != nil {
		w.Write([]byte(err.Error()))
	} else {
		w.Write(stateJSON)
	}
}

func getState(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	state := _getState()
	json, err := json.Marshal(state)
	if err != nil {
		w.Write([]byte(err.Error()))
	}
	w.Write(json)
}

func setupResponse(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}
