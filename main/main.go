package main

import (
	"encoding/json"
	"encoding/xml"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
)

var config struct {
	start        int
	nbPlayers    int
	isProd       bool
	isInflux     bool
	isValidState bool
}

func init() {
	flag.IntVar(&config.start, "start", 0, "Where start to request")
	flag.IntVar(&config.nbPlayers, "range", 16000, "Nb players to request")
	flag.BoolVar(&config.isProd, "prod", false, "Is in prod env")
	flag.BoolVar(&config.isValidState, "begin", false, "Is needed to begin directly")
	flag.BoolVar(&config.isInflux, "disable-influx", false, "Set false to disable Local InfluxDb")
	flag.StringVar(&thisUsersHash, "hash", "Still not initialized", "Set ThisUserHash to request players")
	flag.Parse()
	fmt.Printf("%+v, thisuserhash=%s\n", config, thisUsersHash)
}

func makeIDTab(start int, end int) {
	client := &http.Client{
		CheckRedirect: nil,
	}
	req, err := http.NewRequest("GET", "http://fr-do.gamigo.com/world1/highscore.php?von="+strconv.Itoa(start)+"&mode=1&webservice=true&end="+strconv.Itoa(end), nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Add("cookie", cookie)
	req.Header.Add("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	txtResp, _ := ioutil.ReadAll(resp.Body)
	var f struct {
		Data string `json:"data"`
	}
	err = json.Unmarshal(txtResp, &f)
	xmlStr := "<s>" + strings.Replace(strings.Replace(f.Data, "\\", "", -1), "&nbsp;*&nbsp;", "", -1) + "</s>"
	var ret struct {
		Users []struct {
			IDs []struct {
				ID string `xml:"href,attr"`
			} `xml:"td>a"`
		} `xml:"tr"`
	}
	err = xml.Unmarshal([]byte(xmlStr), &ret)
	for i := range ret.Users {
		idTab[start] = ret.Users[i].IDs[0].ID[21:]
		start++
	}
}

var idTab []string

func checkRequest(id string) error {
	var err error

	if err = requestPlayer(id); err == nil {
		return nil
	}
	if err = requestPlayer(id); err == nil {
		return nil
	}
	return errors.New("Fail request ID:" + id)
}

func main() {
	cookie = sessID + thisUsersHash
	idTab = make([]string, config.nbPlayers, config.nbPlayers)
	ticker := time.Tick(time.Second / 4)
	if config.isValidState == true {
		fmt.Println("Build IdTab: Loading...")
		makeIDTab(config.start, config.start+config.nbPlayers/2)
		makeIDTab(config.start+config.nbPlayers/2-1, config.nbPlayers)
		fmt.Println("Build IdTab: OK")
		fmt.Println("Crawl players : Start")
	} else {
		fmt.Println("Waiting Hash")
	}
	go func() {
		for {
			nbFails := 0
			for i := 0; config.isValidState && i < config.nbPlayers; i++ {
				select {
				case <-ticker:
					go func(id int) {
						if err := checkRequest(idTab[id]); err != nil {
							nbFails++
							if nbFails > 80 {
								fmt.Println("Crawler is now stoped due too many fails")
								config.isValidState = false
							}
							fmt.Println(err)
						} else {
							nbFails = 0
						}
					}(i)
				}
			}
		}
	}()

	r := mux.NewRouter()
	playersR := r.PathPrefix("/players").Subrouter()
	utilsR := r.PathPrefix("/utils").Subrouter()

	playersR.HandleFunc("/list", playersList)
	playersR.HandleFunc("/get-infos/{id:[0-9a-z]+}", playersGetInfos)

	utilsR.HandleFunc("/set-cookie", setCookie)
	utilsR.HandleFunc("/get-state", getState)
	http.Handle("/", r)

	http.ListenAndServe("localhost:8013", nil)
}
