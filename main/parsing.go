package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"./logs"
	"github.com/fatih/structs"
	"github.com/influxdata/influxdb/client/v2"
	"golang.org/x/net/html"
)

type batsLogs struct {
	Raffs  logs.Log
	Mines  logs.Log
	Munis  logs.Log
	Usines logs.Log
}

type infos struct {
	Bats                   batsLogs
	Name                   string
	ID                     string
	ForCastPoints          logs.Log
	ForCastPointsSansMunis logs.Log
	Points                 logs.Log
	DeltaPoint             float64
	Rank                   int
	PointsCombat           int
	RankCombat             int
	Since                  string
	Perte                  int
	Win                    int
	Lose                   int
	Ban                    bool
	Co                     bool
	time                   time.Time
}

type infosToInflux struct {
	BatsRaffsEx                int
	BatsRaffsMant              int
	BatsMinesEx                int
	BatsMinesMant              int
	BatsUsinesEx               int
	BatsUsinesMant             int
	BatsMunisEx                int
	BatsMunisMant              int
	PointsEx                   int
	PointsMant                 int
	ForCastPointsEx            int
	ForCastPointsMant          int
	ForCastPointsSansMunisEx   int
	ForCastPointsSansMunisMant int
	PointsCombatEx             int
	PointsCombatMant           int
	RankCombat                 int
	Since                      string
	Perte                      int
	Win                        int
	Lose                       int
	Ban                        bool
	Co                         bool
	DeltaPoint                 float64
	Rank                       int
	Name                       string
	ID                         string
}

type infosLogToInflux struct {
	BatsRaffs              logs.Log
	BatsMines              logs.Log
	BatsUsines             logs.Log
	BatsMunis              logs.Log
	Points                 logs.Log
	ForCastPoints          logs.Log
	ForCastPointsSansMunis logs.Log
	PointsCombat           int
	RankCombat             int
	Since                  string
	Perte                  int
	Win                    int
	Lose                   int
	Ban                    bool
	Co                     bool
	DeltaPoint             float64
	Rank                   int
	Name                   string
	ID                     string
}

var m sync.Map
var cleanStrRegExp = regexp.MustCompile("\u00a0|,")

func transInfosToInflux(i *infos) map[string]interface{} {
	ret := new(infosLogToInflux)
	ret.BatsMines = i.Bats.Mines
	ret.BatsUsines = i.Bats.Usines
	ret.BatsRaffs = i.Bats.Raffs
	ret.BatsMunis = i.Bats.Munis
	ret.Points = i.Points
	ret.PointsCombat = i.PointsCombat
	ret.ForCastPoints = i.ForCastPoints
	ret.ForCastPointsSansMunis = i.ForCastPointsSansMunis
	ret.RankCombat = i.RankCombat
	ret.Rank = i.Rank
	ret.ID = i.ID
	ret.Name = i.Name
	ret.Co = i.Co
	ret.DeltaPoint = i.DeltaPoint
	ret.Since = i.Since
	ret.Lose = i.Lose
	ret.Win = i.Win
	return structs.Map(ret)
}

func pushInDb(i infos) {
	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  db,
		Precision: "µs",
	})
	if err != nil {
		log.Fatal(err)
	}
	pt, err := client.NewPoint(i.ID[:10], nil, transInfosToInflux(&i), i.time)
	if err != nil {
		log.Fatal(err)
	}
	bp.AddPoint(pt)
	if err := c.Write(bp); err != nil {
		log.Fatal(err)
	}
}

func findHead(tab []*html.Node) []int {
	ret := make([]int, 3, 3)
	i := 0
	for k, e := range tab {
		if e.Attr != nil && e.Attr[0].Val == "head" {
			ret[i] = k
			i++
		}
	}
	return ret
}

func forCastPointsLogs(bats *batsLogs) logs.Log {
	tabPts := make([]logs.Log, 4)
	ret := logs.Log(0)
	tabPts[0] = bats.Usines + logs.Log(math.Log(1000))
	tabPts[1] = bats.Mines + logs.Log(math.Log(80))
	tabPts[2] = bats.Munis + logs.Log(math.Log(300))
	tabPts[3] = bats.Raffs + logs.Log(math.Log(125))
	ret = tabPts[3]
	for i := 0; i < 3; i++ {
		ret = logs.AddLogs(ret, tabPts[i])
	}
	return ret
}

func divCust(a logs.Log, b logs.Log) float64 {
	return float64(a - b)
}

func cleanStr(s string) string {
	return cleanStrRegExp.ReplaceAllLiteralString(s, "")
}

func getLogFromNode(n *html.Node) (logs.Log, error) {
	if len(n.Attr) > 2 {
		tmp := n.Attr[2].Val
		return logs.TransformStringIntoLogs(cleanStr(tmp)), nil
	}
	return logs.TransformStringIntoLogs(cleanStr(n.Data)), nil
}

func getFNF(n *html.Node) *html.Node {
	return n.FirstChild.NextSibling.FirstChild
}

func fillBats(b *[]*html.Node, tabHead *[]int, body *infos, shiftName int, shiftMunis int) error {
	var err error
	bats := &(body.Bats)
	getFNF((*b)[(*tabHead)[2]+1])
	bats.Usines, err = getLogFromNode(getFNF((*b)[(*tabHead)[2]+1]))
	if shiftMunis == 1 {
		bats.Munis, err = getLogFromNode(getFNF((*b)[(*tabHead)[2]+2]))
	}
	bats.Mines, err = getLogFromNode(getFNF((*b)[(*tabHead)[2]+2+shiftMunis]))
	bats.Raffs, err = getLogFromNode(getFNF((*b)[(*tabHead)[2]+3+shiftMunis]))
	body.Points, err = getLogFromNode(getFNF((*b)[(*tabHead)[2]+5+shiftMunis]))
	return err
}

func fillNodes(n *html.Node, b *[]*html.Node, body *infos) error {
	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "tr" {
			*b = append(*b, n)
		} else if n.Type == html.ElementNode && n.Data == "img" {
			if n.Attr[1].Key == "src" && n.Attr[1].Val == "images/classic/icons/bullet_green.png" {
				body.Co = true
			} else if n.Attr[1].Key == "src" && n.Attr[1].Val == "images/classic/icons/lock.png" {
				body.Ban = true
			}
		} else {
			for o := n.FirstChild; o != nil; o = o.NextSibling {
				f(o)
			}
		}
		if n.Data == "h1" && n.Attr != nil && n.Attr[0].Val == "main blockHead ltr" {
			body.Name = n.FirstChild.Data
		}
	}
	f(n)
	return nil
}

func fillInfo(n *html.Node, id string) error {
	time := time.Now()
	var body infos
	b := make([]*html.Node, 0, 30)
	ok := fillNodes(n, &b, &body)
	if ok != nil {
		return ok
	}
	tabHead := findHead(b)
	lenB := len(b)
	if lenB < 6 {
		return errors.New("Request failled on ID:" + id)
	}
	body.time = time
	body.ID = id
	shiftName := 0
	shiftMunis := 1
	if b[tabHead[1]+3].FirstChild != nil && b[tabHead[1]+3].FirstChild.NextSibling != nil &&
		getFNF(b[tabHead[1]+3]) != nil &&
		strings.Contains(getFNF(b[tabHead[1]+3]).Data, ".") {
		shiftName++
	}
	if lenB-tabHead[2] != 8 {
		shiftMunis = 0
	}
	body.Rank, err = strconv.Atoi(b[1].FirstChild.NextSibling.NextSibling.FirstChild.FirstChild.FirstChild.Data[1:])
	body.RankCombat, _ = strconv.Atoi(getFNF(b[1].FirstChild.NextSibling.NextSibling.FirstChild).Data[1:])
	tmp := getFNF(b[tabHead[1]+3+shiftName]).Data
	body.Win, _ = strconv.Atoi(tmp[:(strings.Index(tmp, " "))])
	tmp = getFNF(b[tabHead[1]+4+shiftName]).Data
	body.Lose, _ = strconv.Atoi(tmp[:(strings.Index(tmp, " "))])
	tmp = getFNF(b[tabHead[2]+4+shiftMunis]).Data
	body.Perte, _ = strconv.Atoi(tmp[:(strings.Index(tmp, "%"))])
	ok = fillBats(&b, &tabHead, &body, shiftName, shiftMunis)
	if ok != nil {
		return ok
	}
	tmp = getFNF(b[tabHead[2]+5+shiftMunis]).NextSibling.NextSibling.NextSibling.NextSibling.Data
	body.PointsCombat, err = strconv.Atoi(cleanStr(tmp))
	body.ForCastPoints = forCastPointsLogs(&body.Bats)
	tmpBats := body.Bats
	tmpBats.Munis = 0
	body.ForCastPointsSansMunis = forCastPointsLogs(&tmpBats)
	body.DeltaPoint = divCust(body.Points, body.ForCastPoints)
	m.Store(id[:10], body)
	if !config.isInflux {
		pushInDb(body)
	}
	return nil
}

func requestPlayer(id string) error {
	client := &http.Client{
		CheckRedirect: nil,
	}
	req, err := http.NewRequest("GET", "http://fr-do.gamigo.com/world1/userdetails.php?user="+id, nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Add("cookie", cookie)
	req.Header.Add("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	txtRsp, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	doc, err := html.Parse(strings.NewReader(string(txtRsp)))
	ok := fillInfo(doc, id)
	return ok
}

func checkPair(t []string) bool {
	l := config.nbPlayers
	for i := 0; i < l; i++ {
		for j := i; j < l; j++ {
			if i != j && t[i] == t[j] {
				fmt.Println("Tab[", i, "] = ", t[i], "Tab[", j, "] = ", t[j])
				return true
			}
		}
	}
	return false
}
