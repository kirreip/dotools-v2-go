package main

import (
	"fmt"
	"time"
)

func main() {
	tick := time.Tick(time.Hour / 14400)
	go func() {
		for {
			select {
			case <-tick:
				fmt.Println("Test toutes les secs")
			}
		}
	}()
	fmt.Print(time.Hour / 14400)
	time.Sleep(4 * time.Second)
}
