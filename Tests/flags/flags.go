package main

import (
	"flag"
	"fmt"
)

var thisUserHash string

var config struct {
	start     int
	nbPlayers int
	isProd    bool
}

func init() {
	flag.IntVar(&config.start, "start", 0, "Where start to request")
	flag.IntVar(&config.nbPlayers, "players", 16000, "Nb players to request")
	flag.BoolVar(&config.isProd, "prod", false, "Is in prod env")
	flag.StringVar(&thisUserHash, "hash", "Still not initialized", "Set ThisUserHash to request players")
}

func main() {
	flag.Parse()
	fmt.Printf("%+v, %s\n", config, thisUserHash)
}
