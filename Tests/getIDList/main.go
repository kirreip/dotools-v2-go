package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"runtime"
	"strconv"
	"strings"
	"time"

	"golang.org/x/net/html"
)

var cookie = "PHPSESSID=19f5276d6d99567435ff78ae5736172b; thisUsersLandId=564532; thisUsersHash=04595732082fadfc5ad7b226b5961f2f"

func requestPlayer(idW int, i int) {
	start := time.Now()
	client := &http.Client{
		CheckRedirect: nil,
	}
	req, err := http.NewRequest("GET", "https://game.desert-operations.fr/world1/userdetails.php?user="+strconv.Itoa(i), nil)
	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("cookie", cookie)
	req.Header.Add("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	txtResp, _ := ioutil.ReadAll(resp.Body)
	doc, err := html.Parse(strings.NewReader(string(txtResp)))
	if err != nil {
		log.Fatal(err)
	}
	if doc.FirstChild.NextSibling.NextSibling.FirstChild.NextSibling.FirstChild.FirstChild.Data == "div" {
		fmt.Println("Worker#", idW, "found: ", i, "in ", time.Since(start))
	}
}

func worker(id int, jobs <-chan int) {
	for j := range jobs {
		requestPlayer(id, j)
	}
}

func main() {
	j := make(chan int)
	max := runtime.GOMAXPROCS(1)
	fmt.Println("Max Core:", max)
	for i := 0; i < max; i++ {
		go worker(i, j)
	}
	for i := 0; i < 500000; i++ {
		j <- i
	}
}
