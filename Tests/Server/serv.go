package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type requestClient struct {
	Start int
	End   int
}

func setupResponse(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

func handleRoot(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	var request requestClient
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
	}
	err = json.Unmarshal(body, &request)
	fmt.Fprintf(w, "%d\n", request.End+request.Start)
}

func main() {
	fmt.Println("Starting server")
	http.HandleFunc("/", handleRoot)
	http.ListenAndServe(":8013", nil)
}
