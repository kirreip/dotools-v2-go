package main

import (
	"fmt"
	"log"
	"time"

	"github.com/fatih/structs"
	"github.com/influxdata/influxdb/client/v2"
)

const (
	db = "mydb"
)

type testStruct struct {
	T1 int
	T2 string
	T3 bool
}

func main() {
	c, err := client.NewHTTPClient(client.HTTPConfig{
		Addr:     "http://localhost:8086",
		Username: "test",
		Password: "test2314",
	})
	testR := &testStruct{122, "efefefef", true}
	if err != nil {
		log.Fatal(err)
	}
	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  db,
		Precision: "µs",
	})
	if err != nil {
		log.Fatal(err)
	}
	fields := map[string]interface{}{
		"idle":   10.1,
		"system": 53.3,
		"user":   564.45,
	}
	if fields != nil {
	}
	if &testR != nil {
	}
	sm := structs.Map(testR)
	fmt.Println("sm:", sm)
	pt, err := client.NewPoint("cpu_usage", nil, sm, time.Now())
	if err != nil {
		log.Fatal(err)
	}
	bp.AddPoint(pt)
	if err := c.Write(bp); err != nil {
		log.Fatal(err)
	}
}
