package main

import (
	"encoding/xml"
	"fmt"
)

type ret struct {
	TabID []struct {
		ID    string `xml:"href,attr"`
		Class string `xml:"class,attr"`
	} `xml:"tr>td>a"`
}

type ret2 struct {
	Users []struct {
		ID []struct {
			ID string `xml:"href,attr"`
		} `xml:"td>a"`
	} `xml:"tr"`
}

func main() {
	v := ret2{}
	if err := xml.Unmarshal([]byte(data), &v); err != nil {
		fmt.Println(err)
		return
	}
	for i := range v.Users {
		fmt.Println(v.Users[i].ID[0].ID)
	}
}
