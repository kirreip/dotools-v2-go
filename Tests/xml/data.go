package main

const data = `
<tr class="even" data-place="1">
	<td class="td1">
		<strong>01</strong>
	</td>
	<td class="showTooltipDefault td2" title="Classement combat">09</td>
	<td class="td3">
		<a class="ancUserDetail openUserDetails" href="userdetails.php?user=e16de2736c69119aec44da36b9b253ee1098f5a11bc0879e83022744dd869880">
			<strong>david21-21</strong>
		</a>
		<img class="userStatusImage showTooltipDefault" src="images/classic/icons/bullet_green.png" alt="Est en ligne" title="Est en ligne"/>
	</td>
	<td class="td4">
		<a class="openAllianceDetails" href="alliancedetails.php?ally=45068">LES LEGENDAIRES</a>
	</td>
	<td class="alignRight td5">
		<span class="tooltipExtention showTooltipDefault" title="44,493,989,090,328,588,518,521,150,516,316,399,938,474,982,967,267,964,518,563,776,605,745,269,665,341,486,342" data="44,493,989,090,328,588,518,521,150,516,316,399,938,474,982,967,267,964,518,563,776,605,745,269,665,341,486,342">44,494;10																						
			<sup>
				<span style="font-size: 0">^</span>78
			</sup>
		</span>
		<img src="images/classic/icon_punkte.png" alt=""/>
	</td>
</tr>
<tr class="odd" data-place="2">
	<td class="td1">
		<strong>02</strong>
	</td>
	<td class="showTooltipDefault td2" title="Classement combat">140</td>
	<td class="td3">
		<a class="ancUserDetail openUserDetails" href="userdetails.php?user=28f5025a8a9fdb7472de0a8f3eef5f3a232390054cdf193c728b1808b0e8d2e6">
			<strong>Thydegage</strong>
		</a>
		<img class="userStatusImage showTooltipDefault" src="images/classic/icons/bullet_green.png" alt="Est en ligne" title="Est en ligne"/>
	</td>
	<td class="td4">
		<a class="openAllianceDetails" href="alliancedetails.php?ally=45972">OSEF</a>
	</td>
	<td class="alignRight td5">
		<span class="tooltipExtention showTooltipDefault" title="17,818,682,834,752,426,075,157,946,268,261,615,696,168,885,362,156,252,603,004,376,253,459,663,772,833,460,671" data="17,818,682,834,752,426,075,157,946,268,261,615,696,168,885,362,156,252,603,004,376,253,459,663,772,833,460,671">17,819;10																																																
			<sup>
				<span style="font-size: 0">^</span>78
			</sup>
		</span>
		<img src="images/classic/icon_punkte.png" alt=""/>
	</td>
</tr>
<tr class="even" data-place="3">
	<td class="td1">
		<strong>03</strong>
	</td>
	<td class="showTooltipDefault td2" title="Classement combat">39</td>
	<td class="td3">
		<a class="ancUserDetail openUserDetails" href="userdetails.php?user=0e9f35fece89f26ce546383d98c577483704a7739ee201d45496c4ca1fd761c2">
			<strong>Hacene13</strong>
		</a>
		<img class="userStatusImage showTooltipDefault" src="images/classic/icons/bullet_green.png" alt="Est en ligne" title="Est en ligne"/>
	</td>
	<td class="td4">
		<a class="openAllianceDetails" href="alliancedetails.php?ally=45997">LE CANTONNIER</a>
	</td>
	<td class="alignRight td5">
		<span class="tooltipExtention showTooltipDefault" title="15,572,896,181,615,005,981,482,402,680,710,739,978,466,244,038,543,787,581,497,321,812,010,844,408,409,304,911" data="15,572,896,181,615,005,981,482,402,680,710,739,978,466,244,038,543,787,581,497,321,812,010,844,408,409,304,911">15,573;10																																																																										
			<sup>
				<span style="font-size: 0">^</span>78
			</sup>
		</span>
		<img src="images/classic/icon_punkte.png" alt=""/>
	</td>
</tr>
<tr class="odd" data-place="4">
	<td class="td1">
		<strong>04</strong>
	</td>
	<td class="showTooltipDefault td2" title="Classement combat">449</td>
	<td class="td3">
		<a class="ancUserDetail openUserDetails" href="userdetails.php?user=cd2f9df512a1e7f7ed6df7ac62d211eea8e33f8a8d60e6755174851d89936e9a">
			<strong>NanoNico</strong>
		</a>
		<img class="userStatusImage showTooltipDefault" src="images/classic/icons/bullet_green.png" alt="Est en ligne" title="Est en ligne"/>
	</td>
	<td class="td4">
		<a class="openAllianceDetails" href="alliancedetails.php?ally=45681">S_I</a>
	</td>
	<td class="alignRight td5">
		<span class="tooltipExtention showTooltipDefault" title="13,025,675,555,332,619,753,737,785,536,888,067,745,166,706,202,537,044,112,662,042,321,685,244,052,373,582,546" data="13,025,675,555,332,619,753,737,785,536,888,067,745,166,706,202,537,044,112,662,042,321,685,244,052,373,582,546">13,026;10																																																																																																				
			<sup>
				<span style="font-size: 0">^</span>78
			</sup>
		</span>
		<img src="images/classic/icon_punkte.png" alt=""/>
	</td>
</tr>
<tr class="even" data-place="5">
	<td class="td1">
		<strong>05</strong>
	</td>
	<td class="showTooltipDefault td2" title="Classement combat">244</td>
	<td class="td3">
		<a class="ancUserDetail openUserDetails" href="userdetails.php?user=bd9bf7c7f3e5eb39afbbcc23bbda9ae35e44400b7e3267bab559f83e6adbf6e9">
			<strong>montgomeryy</strong>
		</a>
		<img class="userStatusImage showTooltipDefault" src="images/classic/icons/bullet_green.png" alt="Est en ligne" title="Est en ligne"/>
	</td>
	<td class="td4">
		<a class="openAllianceDetails" href="alliancedetails.php?ally=45027">LES SICARIOS</a>
	</td>
	<td class="alignRight td5">
		<span class="tooltipExtention showTooltipDefault" title="12,727,630,596,251,732,919,789,050,377,817,820,504,341,224,340,401,608,829,921,670,004,777,228,596,844,051,161" data="12,727,630,596,251,732,919,789,050,377,817,820,504,341,224,340,401,608,829,921,670,004,777,228,596,844,051,161">12,728;10																																																																																																																														
			<sup>
				<span style="font-size: 0">^</span>78
			</sup>
		</span>
		<img src="images/classic/icon_punkte.png" alt=""/>
	</td>
</tr>
<tr class="odd" data-place="6">
	<td class="td1">
		<strong>06</strong>
	</td>
	<td class="showTooltipDefault td2" title="Classement combat">959</td>
	<td class="td3">
		<a class="ancUserDetail openUserDetails" href="userdetails.php?user=c9f5238ed98c7894d84697ce1e36e62f0d35091b37cb7f7b9a004325e00e9b23">
			<strong>shuriko</strong>
		</a>
	</td>
	<td class="td4">
		<a class="openAllianceDetails" href="alliancedetails.php?ally=45883">P.D.C</a>
	</td>
	<td class="alignRight td5">
		<span class="tooltipExtention showTooltipDefault" title="10,691,209,700,851,455,621,704,068,209,207,769,274,896,874,578,333,091,228,763,423,112,961,418,539,967,261,451" data="10,691,209,700,851,455,621,704,068,209,207,769,274,896,874,578,333,091,228,763,423,112,961,418,539,967,261,451">10,691;10																																																																																																																																																								
			<sup>
				<span style="font-size: 0">^</span>78
			</sup>
		</span>
		<img src="images/classic/icon_punkte.png" alt=""/>
	</td>
</tr>
<tr class="even" data-place="7">
	<td class="td1">
		<strong>07</strong>
	</td>
	<td class="showTooltipDefault td2" title="Classement combat">556</td>
	<td class="td3">
		<a class="ancUserDetail openUserDetails" href="userdetails.php?user=da10364b608f6e912cd4d42bfff6f45f1ff4cc6520e87eed328e439c585f7c90">
			<strong>Gohan17</strong>
		</a>
	</td>
	<td class="td4">
		<a class="openAllianceDetails" href="alliancedetails.php?ally=44603">SPECTRE</a>
	</td>
	<td class="alignRight td5">
		<span class="tooltipExtention showTooltipDefault" title="10,691,209,700,851,455,621,704,042,246,548,286,096,867,019,397,048,914,414,225,379,845,326,287,340,917,607,501" data="10,691,209,700,851,455,621,704,042,246,548,286,096,867,019,397,048,914,414,225,379,845,326,287,340,917,607,501">10,691;10																																																																																																																																																																																		
			<sup>
				<span style="font-size: 0">^</span>78
			</sup>
		</span>
		<img src="images/classic/icon_punkte.png" alt=""/>
	</td>
</tr>
<tr class="odd" data-place="8">
	<td class="td1">
		<strong>08</strong>
	</td>
	<td class="showTooltipDefault td2" title="Classement combat">-</td>
	<td class="td3">
		<a class="ancUserDetail openUserDetails" href="userdetails.php?user=965c34f70cbd8b8ab17d95d5b39a97e89bf5bf787a2a2ec517f40dd257a3fb13">
			<strong>la fu00e9e</strong>
		</a>
	</td>
	<td class="td4">
		<a class="openAllianceDetails" href="alliancedetails.php?ally=43464">Les Gentlemen</a>
	</td>
	<td class="alignRight td5">
		<span class="tooltipExtention showTooltipDefault" title="8,686,607,881,941,809,742,667,249,169,070,125,318,746,270,151,742,776,918,108,012,549,870,311,339,564,357,336" data="8,686,607,881,941,809,742,667,249,169,070,125,318,746,270,151,742,776,918,108,012,549,870,311,339,564,357,336">8,687;10																																																																																																																																																																																																												
			<sup>
				<span style="font-size: 0">^</span>78
			</sup>
		</span>
		<img src="images/classic/icon_punkte.png" alt=""/>
	</td>
</tr>
<tr class="even" data-place="9">
	<td class="td1">
		<strong>09</strong>
	</td>
	<td class="showTooltipDefault td2" title="Classement combat">34</td>
	<td class="td3">
		<a class="ancUserDetail openUserDetails" href="userdetails.php?user=3cb36c19623b35023079c57aaab3937e045b5bce8f37565bcf84b2d615bef2fb">
			<strong>Hannibal_Lecter</strong>
		</a>
	</td>
	<td class="td4">
		<a class="openAllianceDetails" href="alliancedetails.php?ally=45997">LE CANTONNIER</a>
	</td>
	<td class="alignRight td5">
		<span class="tooltipExtention showTooltipDefault" title="8,524,500,042,542,862,450,687,960,011,444,816,367,812,010,127,867,104,082,591,896,528,792,109,652,824,267,551" data="8,524,500,042,542,862,450,687,960,011,444,816,367,812,010,127,867,104,082,591,896,528,792,109,652,824,267,551">8,524;10																																																																																																																																																																																																																																						
			<sup>
				<span style="font-size: 0">^</span>78
			</sup>
		</span>
		<img src="images/classic/icon_punkte.png" alt=""/>
	</td>
</tr>
<tr class="odd" data-place="10">
	<td class="td1">
		<strong>10</strong>
	</td>
	<td class="showTooltipDefault td2" title="Classement combat">234</td>
	<td class="td3">
		<a class="ancUserDetail openUserDetails" href="userdetails.php?user=f38a98070f64bea1c8f24a4cb8772ae8f0f7ed21f4ae3ec3808f7f07938f7b41">
			<strong>gabi-eden</strong>
		</a>
		<img class="userStatusImage showTooltipDefault" src="images/classic/icons/bullet_green.png" alt="Est en ligne" title="Est en ligne"/>
	</td>
	<td class="td4">
		<a class="openAllianceDetails" href="alliancedetails.php?ally=44992">B-H</a>
	</td>
	<td class="alignRight td5">
		<span class="tooltipExtention showTooltipDefault" title="6,295,170,413,458,808,059,079,998,573,402,458,452,375,815,592,882,745,864,657,865,820,742,112,754,670,745,166" data="6,295,170,413,458,808,059,079,998,573,402,458,452,375,815,592,882,745,864,657,865,820,742,112,754,670,745,166">6,295;10																																																																																																																																																																																																																																																																
			<sup>
				<span style="font-size: 0">^</span>78
			</sup>
		</span>
		<img src="images/classic/icon_punkte.png" alt=""/>
	</td>
</tr>
`
