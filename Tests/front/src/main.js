import Vue from "vue";
import App from "./App.vue";
import HelloWorld from "./components/HelloWorld.vue";
import TestA from "./components/TestA.vue";
import TestAA from "./components/TestAA.vue";
import TestAB from "./components/TestAB.vue";

import VueMaterial from "vue-material";
import VueRouter from "vue-router";
import "vue-material/dist/vue-material.min.css";

Vue.use(VueMaterial);
Vue.use(VueRouter);
const routes = [
  {
    path: "/test-a",
    name: "testA",
    component: TestA,
    children: [
      {
        path: "testAA",
        name: "testAA",
        component: TestAA
      },
      {
        path: "testAB",
        name: "testAB",
        component: TestAB
      }
    ]
  },
  { path: "/hello-world", name: "HelloWorld", component: HelloWorld }
];

const router = new VueRouter({
  mode: 'history',
  routes
});

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  router
}).$mount("#app");
