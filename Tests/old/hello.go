package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"strconv"
	"strings"
	"time"

	"golang.org/x/net/html"
)

const (
	_ = iota
)

type ex struct {
	mant int
	ex   int
}

type bats struct {
	raffs  ex
	mines  ex
	munis  ex
	usines ex
}

type infos struct {
	Bats                   bats
	Name                   string
	ID                     string
	ForCastPoints          ex
	ForCastPointsSansMunis ex
	Points                 ex
	DeltaPoint             float64
	Rank                   int
	PointsCombat           ex
	RankCombat             int
	Since                  string
	Perte                  int
	Win                    int
	Lose                   int
	Ban                    bool
	Co                     bool
}

func lol(jobs <-chan int, c chan int, id int) {
	i := 0
	for {
		select {
		case <-jobs:
			i++
			fmt.Printf("tick 100 #%d ##%d\n", id, i)
		case <-c:
			return
		}
	}
}

func putJobs(tick <-chan time.Time, jobs chan int, nbW int) {
	for {
		select {
		case <-tick:
			for i := 0; i < nbW; i++ {
				jobs <- 1
			}
		}
	}
}

func transform(nb string) ex {
	len := len(nb)
	var ret ex
	if len < 7 {
		ret.ex = 0
		ret.mant, _ = strconv.Atoi(nb)
	} else {
		ret.ex = int((math.Floor((float64(len)-1.0)/3.0) * 3.0)) - 3
		mod := len % 3
		if mod == 0 {
			mod = 6
		} else {
			mod = mod + 3
		}
		ret.mant, _ = strconv.Atoi(nb[:mod])
	}
	return ret
}

func clean(e *ex) {
	for e.mant > 999999 {
		e.mant /= 1000
		e.ex += 3
	}
}

func forCastPoints(bats *bats) ex {
	maxEx := 0
	TabPts := make([]ex, 4)
	ret := ex{ex: 0, mant: 0}
	TabPts[3] = ex{mant: bats.raffs.mant * 125, ex: bats.raffs.ex}
	TabPts[0] = ex{mant: bats.usines.mant * 1000, ex: bats.usines.ex}
	TabPts[1] = ex{mant: bats.mines.mant * 80, ex: bats.mines.ex}
	TabPts[2] = ex{mant: bats.munis.mant * 300, ex: bats.munis.ex}
	clean(&TabPts[0])
	clean(&TabPts[1])
	clean(&TabPts[2])
	clean(&TabPts[3])
	for i := 0; i < 4; i++ {
		if TabPts[i].ex > maxEx {
			maxEx = TabPts[i].ex
		}
	}
	ret.ex = maxEx
	for i := 0; i < 4; i++ {
		if TabPts[i].ex == maxEx {
			ret.mant += TabPts[i].mant
		} else if math.Abs(float64(TabPts[i].ex-maxEx)) == 3 {
			ret.mant += TabPts[i].mant / 1000
		}
	}
	clean(&ret)
	return ret
}

func divCust(a *ex, b *ex) float64 {
	if a.ex == b.ex {
		return (float64(a.mant) / float64(b.mant))
	} else if math.Abs(float64(a.ex-b.ex)) == 3 {
		return ((float64(a.mant) * math.Pow(10, float64(a.ex%10))) / (float64(b.mant) * math.Pow(10, float64(b.ex%10))))
	} else if a.ex > b.ex {
		return 100
	} else {
		return 0
	}
}

func getBody(doc *html.Node) ([]*html.Node, error) {
	b := make([]*html.Node, 0, 15000)
	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "tr" {
			// fmt.Println(n)
			b = append(b, n)
		} else {
			for c := n.FirstChild; c != nil; c = c.NextSibling {
				// fmt.Println(c)
				f(c)
			}
		}
	}
	f(doc)
	if len(b) != 0 {
		b = b[1:]
		return b, nil
	}
	return nil, errors.New("Cookie ou Fail Request")
}

func renderNode(n []*html.Node) []infos {
	var f func(*html.Node)
	inf := make([]infos, 15000, 15000)
	var i int
	f = func(c *html.Node) {
		// fmt.Println("I LOOOOL: ", strconv.Itoa(i))
		if c.Attr != nil {
			if c.Attr[0].Key == "class" {
				if c.Attr[0].Val == "td1" {
					if c.FirstChild.FirstChild != nil {
						fmt.Println("Classement: " + c.FirstChild.FirstChild.Data)
						inf[i].rank, _ = strconv.Atoi(c.FirstChild.FirstChild.Data)
					} else {
						fmt.Println("Classement: " + c.FirstChild.Data)
						inf[i].rank, _ = strconv.Atoi(c.FirstChild.Data)
					}
				} else if c.Attr[0].Val == "showTooltipDefault td2" {
					fmt.Println("Classement Combat: " + c.FirstChild.Data)
					inf[i].rankCombat, _ = strconv.Atoi(c.FirstChild.Data)
				} else if c.Attr[0].Val == "td3" {
					inf[i].id = c.FirstChild.Attr[1].Val
					inf[i].id = inf[i].id[21:]
					inf[i].name = c.FirstChild.FirstChild.FirstChild.Data
					fmt.Println("Id: " + c.FirstChild.Attr[1].Val)
					fmt.Println("Name: " + c.FirstChild.FirstChild.FirstChild.Data)
				} else if c.Attr[0].Val == "td4" {
					if c.FirstChild.FirstChild != nil {
						fmt.Println("Id Alliance: " + c.FirstChild.Attr[1].Val)
						fmt.Println("Name Alliance: " + c.FirstChild.FirstChild.Data)
					}
				}
			}
		}
		for o := c.FirstChild; o != nil; o = o.NextSibling {
			f(o)
		}
	}

	for i = range n {
		fmt.Println("i: " + strconv.Itoa(i))
		f(n[i])
	}
	return inf
}

func fillInfo(body *infos) {
	tmpBats := bats{}
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://game.desert-operations.fr/world1/userdetails.php?user="+body.id, nil)
	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("cookie", cookie)
	req.Header.Add("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	txtResp, _ := ioutil.ReadAll(resp.Body)
	doc, err := html.Parse(strings.NewReader(string(txtResp)))
	var f func(*html.Node)
	b := make([]*html.Node, 0, 30)
	f = func(n *html.Node) {
		//fmt.Println("LOL")
		if n.Type == html.ElementNode && n.Data == "tr" {
			b = append(b, n)
			//fmt.Println(b)
		} else if n.Type == html.ElementNode && n.Data == "img" {
			if n.Attr[1].Key == "src" && n.Attr[1].Val == "images/classic/icons/bullet_green.png" {
				body.co = true
			} else if n.Attr[1].Key == "src" && n.Attr[1].Val == "images/classic/icons/lock.png" {
				body.ban = true
			}
		} else {
			//fmt.Println("WALLLAH")
			for o := n.FirstChild; o != nil; o = o.NextSibling {
				f(o)
			}
		}
	}
	f(doc)
	sft := 1
	if b[12].FirstChild.FirstChild.Data == "strong" {
		sft = 0
	}
	tmp := b[8].FirstChild.NextSibling.FirstChild.Data
	body.win, _ = strconv.Atoi(tmp[:(strings.Index(tmp, " "))])
	tmp = b[9].FirstChild.NextSibling.FirstChild.Data
	body.lose, _ = strconv.Atoi(tmp[:(strings.Index(tmp, " "))])
	body.since = b[7].FirstChild.NextSibling.FirstChild.Data
	tmp = b[17+sft].FirstChild.NextSibling.FirstChild.Data
	body.perte, _ = strconv.Atoi(tmp[:(strings.Index(tmp, "%"))])
	body.bats.usines = transform(b[13+sft].FirstChild.NextSibling.FirstChild.Data)
	body.bats.mines = transform(b[15+sft].FirstChild.NextSibling.FirstChild.Data)
	body.bats.raffs = transform(b[16+sft].FirstChild.NextSibling.FirstChild.Data)
	body.bats.munis = transform(b[14+sft].FirstChild.NextSibling.FirstChild.Data)
	tmp = b[18+sft].FirstChild.NextSibling.FirstChild.FirstChild.Data
	body.points = transform(strings.Replace(tmp, ",", "", -1))
	tmp = b[18+sft].FirstChild.NextSibling.FirstChild.NextSibling.NextSibling.FirstChild.Data
	body.pointsCombat = transform(strings.Replace(tmp, ",", "", -1))
	body.forCastPoints = forCastPoints(&body.bats)
	tmpBats = body.bats
	tmpBats.munis = ex{ex: 0, mant: 0}
	body.forCastPointsSansMunis = forCastPoints(&tmpBats)
	body.deltaPoint = divCust(&body.points, &body.forCastPoints)
	fmt.Printf("%+v\n\n", body)
}

// var cookie = "PHPSESSID=19f5276d6d99567435ff78ae5736172b; thisUsersLandId=564532; thisUsersHash=030a15e25519194f26c360409ab382b4"

func refresh() {
	ticker := time.Tick(time.Millisecond * 500)
	go func() {
		for {
			select {
			case <-ticker:
				fmt.Println(time.Millisecond * 500)
			}
		}
	}()
	start, end := 0, 10
	client := &http.Client{
		CheckRedirect: nil,
	}
	req, err := http.NewRequest("GET", "http://game.desert-operations.fr/world1/highscore.php?von="+strconv.Itoa(start)+"&mode=1&end="+strconv.Itoa(end), nil)
	if err != nil {
		return
	}
	req.Header.Add("cookie", cookie)
	req.Header.Add("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("LOL SA FAIL\n")
		fmt.Println(err)
		return
	}
	fmt.Println("Fin Request")
	txtResp, _ := ioutil.ReadAll(resp.Body)
	doc, err := html.Parse(strings.NewReader(string(txtResp)))
	if err != nil {
		fmt.Println(err)
		return
	}
	bn, err := getBody(doc)
	if err != nil {
		fmt.Println(err)
		return
	}
	body := renderNode(bn)
	for i := 0; i < end; i++ {
		go fillInfo(&body[i])
	}
	time.Sleep(45 * time.Second)
	return
}

func makeIDTab(b []string, s int, e int) {
	client := &http.Client{
		CheckRedirect: nil,
	}
	req, err := http.NewRequest("GET", "http://game.desert-operations.fr/world1/highscore.php?von="+strconv.Itoa(s)+"&mode=1&end="+strconv.Itoa(e), nil)
	if err != nil {
		fmt.Println("Cookie Failled")
		log.Fatal(err)
	}
	req.Header.Add("cookie", cookie)
	req.Header.Add("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Cookie Failled")
		log.Fatal(err)
	}
	fmt.Println("Fin Request")
	txtResp, _ := ioutil.ReadAll(resp.Body)
	doc, err := html.Parse(strings.NewReader(string(txtResp)))
	if err != nil {
		log.Fatal(err)
	}
	var f func(*html.Node)
	i := 0
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "tr" {
			tr := n.FirstChild.NextSibling.NextSibling.FirstChild.Attr
			if tr == nil {
				return
			}
			b[i] = tr[1].Val[21:]
			i++
		} else {
			for c := n.FirstChild; c != nil; c = c.NextSibling {
				f(c)
			}
		}
	}
	f(doc)
}
