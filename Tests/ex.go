package main

import (
	"math"
	"strconv"
)

func transform(nb string) ex {
	len := len(nb)
	var ret ex
	if len < 7 {
		ret.Ex = 0
		ret.Mant, _ = strconv.Atoi(nb)
	} else {
		ret.Ex = int((math.Floor((float64(len)-1.0)/3.0) * 3.0)) - 3
		mod := len % 3
		if mod == 0 {
			mod = 6
		} else {
			mod = mod + 3
		}
		ret.Mant, _ = strconv.Atoi(nb[:mod])
	}
	return ret
}

func clean(e *ex) {
	for e.Mant > 999999 {
		e.Mant /= 1000
		e.Ex += 3
	}
}

func forCastPoints(bats *bats) ex {
	maxEx := 0
	TabPts := make([]ex, 4)
	ret := ex{Ex: 0, Mant: 0}
	TabPts[3] = ex{Mant: bats.Raffs.Mant * 125, Ex: bats.Raffs.Ex}
	TabPts[0] = ex{Mant: bats.Usines.Mant * 1000, Ex: bats.Usines.Ex}
	TabPts[1] = ex{Mant: bats.Mines.Mant * 80, Ex: bats.Mines.Ex}
	TabPts[2] = ex{Mant: bats.Munis.Mant * 300, Ex: bats.Munis.Ex}
	clean(&TabPts[0])
	clean(&TabPts[1])
	clean(&TabPts[2])
	clean(&TabPts[3])
	for i := 0; i < 4; i++ {
		if TabPts[i].Ex > maxEx {
			maxEx = TabPts[i].Ex
		}
	}
	ret.Ex = maxEx
	for i := 0; i < 4; i++ {
		if TabPts[i].Ex == maxEx {
			ret.Mant += TabPts[i].Mant
		} else if math.Abs(float64(TabPts[i].Ex-maxEx)) == 3 {
			ret.Mant += TabPts[i].Mant / 1000
		}
	}
	clean(&ret)
	return ret
}

func divCust(a *ex, b *ex) float64 {
	if a.Ex == b.Ex {
		return (float64(a.Mant) / float64(b.Mant))
	} else if math.Abs(float64(a.Ex-b.Ex)) == 3 {
		return ((float64(a.Mant) * math.Pow(10, float64(a.Ex%10))) / (float64(b.Mant) * math.Pow(10, float64(b.Ex%10))))
	} else if a.Ex > b.Ex {
		return 100
	}
	return 0
}
