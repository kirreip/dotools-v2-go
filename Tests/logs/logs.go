package main

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

type logs float64

type batsLogs struct {
	Raffs  logs
	Mines  logs
	Munis  logs
	Usines logs
}

func transformIntoLogs(nb string) logs {
	len := len(nb)
	var ret logs
	if len < 7 {
		float, _ := strconv.ParseFloat(nb, 64)
		ret = logs(float)
	} else {
		float, _ := strconv.ParseFloat(nb[:7], 64)
		ret = logs(math.Log10(float))
		ret += logs(len) - 7
	}
	return ret
}

func addLogs(a logs, b logs) logs {
	max := math.Max(float64(a), float64(b))
	min := math.Min(float64(a), float64(b))
	diff := min - max
	if diff > 10 {
		return logs(max)
	}
	return logs(max + math.Log(1+math.Pow(math.E, diff)))
}

func forCastPointsLogs(bats *batsLogs) logs {
	tabPts := make([]logs, 4)
	ret := logs(0)
	tabPts[0] = bats.Usines + logs(math.Log10(1000))
	tabPts[1] = bats.Mines + logs(math.Log10(80))
	tabPts[2] = bats.Munis + logs(math.Log10(300))
	tabPts[3] = bats.Raffs + logs(math.Log10(125))
	ret = tabPts[3]
	for i := 0; i < 3; i++ {
		ret = addLogs(ret, tabPts[i])
	}
	return ret
}

func main() {
	str := "8,553,828,731,046,073,549,418,392,145,783,408,209,585,095,452,013,633,788,070,403,918"
	str = strings.Replace(str, ",", "", -1)
	bats := batsLogs{
		transformIntoLogs("249214657642811095801915960418767509976276146340238856395177614558312689491573"),
		transformIntoLogs("4444952045521755187466934793796918454915781248932029897851841027000000000000000"),
		transformIntoLogs("1408431580203017473394616831967869512179532913668337101479811590"),
		transformIntoLogs("656114919596363341194596927753769697001436274285161740059868860455"),
	}
	fmt.Println(float64(forCastPointsLogs(&bats)) / math.Log(10))
	fmt.Println(float64(transformIntoLogs(str)) / math.Log(10))
}
