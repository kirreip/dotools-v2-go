package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
)

var thisUsersHash = "a02060255a4592b6f5d4ac9cf33ab12a"
var sessID = "PHPSESSID=c99efeaf2764b3c017191fed3abdca1d; thisUsersLandId=564532; ; thisUsersHash="
var cookie = sessID + thisUsersHash

func makeIDTab(t *[]string, start int, end int) {
	client := &http.Client{
		CheckRedirect: nil,
	}
	req, err := http.NewRequest("GET", "https://fr-do.gamigo.com/world1/highscore.php?von="+strconv.Itoa(start)+"&mode=1&webservice=true&end="+strconv.Itoa(end), nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Add("cookie", cookie)
	req.Header.Add("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	txtResp, _ := ioutil.ReadAll(resp.Body)
	var f struct {
		Data string `json:"data"`
	}
	err = json.Unmarshal(txtResp, &f)
	xmlStr := "<s>" + strings.Replace(strings.Replace(f.Data, "\\", "", -1), "&nbsp;*&nbsp;", "", -1) + "</s>"
	var ret struct {
		Users []struct {
			IDs []struct {
				ID string `xml:"href,attr"`
			} `xml:"td>a"`
		} `xml:"tr"`
	}
	err = xml.Unmarshal([]byte(xmlStr), &ret)
	for i := range ret.Users {
		(*t)[start] = ret.Users[i].IDs[0].ID[21:]
		start++
	}
}

var start, rangeu = 0, 100

func main() {
	idTab := make([]string, rangeu, rangeu)

	makeIDTab(&idTab, start, start+rangeu/2)
	makeIDTab(&idTab, start+rangeu/2-1, rangeu)
	for i := 0; i < rangeu; i++ {
		fmt.Println(i, "   ", idTab[i])
	}

}

//15637
