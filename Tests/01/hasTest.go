package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	var m sync.Map

	for j := 0; j < 4; j++ {
		go func(o int) {
			for i := 100000 * o; i < 100000*(o+1); i++ {
				m.Store(i*121500, i*545)
			}
		}(j)
	}
	time.Sleep(time.Second * 5)
	fmt.Println("Init Ok")
	m.Range(func(k, v interface{}) bool {
		fmt.Println("Key:", k, " Value:", v)
		return true
	})
	time.Sleep(time.Second * 5)
}
